import { MagnifyingGlassIcon } from "@radix-ui/react-icons";
import { TextField } from "@radix-ui/themes";
import { SearchProps } from "./ISchool";
import { useEffect, useState } from "react";

function Search({ initialSchools, update }: SearchProps) {
    const [value, setValue] = useState('')
    useEffect(() => {
        if (value) {
            update(initialSchools.filter(s => s.school_name.toLowerCase().startsWith(value)))
        } else {
            update(initialSchools)
        }
    }, [value])
    return (
        <TextField.Root size="3" style={{ width: '300px' }}>
            <TextField.Slot>
                <MagnifyingGlassIcon height="16" width="16" />
            </TextField.Slot>
            <TextField.Input placeholder="Search by school name" onChange={(e) => setValue(e.target.value)} />
        </TextField.Root>
    )
}

export default Search;