import { Table, TextField } from "@radix-ui/themes"
import {ISchool, SchoolProps} from "./ISchool"
import SchoolItem from "./SchoolItem"
import Search from "./Search"

const Schools = (props: SchoolProps) => {
    const schools: ISchool[] = props.schools

    return (
        <div className="table-wrap">
            <h1>NYC schools table</h1>
            <Search update={props.setFilteredSchools} initialSchools={props.initialSchools} />
            <Table.Root variant="surface" className="school-table">
                <Table.Header>
                    <Table.Row data-testid="columns-parent">
                        <Table.ColumnHeaderCell>School Name</Table.ColumnHeaderCell>
                        <Table.ColumnHeaderCell style={{ width: '20px' }}>dbn</Table.ColumnHeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body data-testid="list-parent">
                    {schools.map((s: ISchool) => <SchoolItem key={s.dbn} item={s} />)}
                    
                </Table.Body>
            </Table.Root>
        </div>
    )
}

export default Schools