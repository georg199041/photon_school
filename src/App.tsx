import React, { useEffect, useState } from 'react';
import './App.css';
import Schools from './Schools';
import { ISchool } from './ISchool'

function App() {
  const [schools, setSchools] = useState([] as ISchool[])
  const [loading, setLoading] = useState(false)
  const [filteredSchools, setFilteredSchools] = useState([] as ISchool[])
  useEffect(() => {
    async function asyncFn() {
      setLoading(true)
      const schoolsResult: ISchool[] = await fetch('https://data.cityofnewyork.us/resource/s3k6-pzi2.json').then(data => data.json())
      setSchools(schoolsResult)
      setFilteredSchools(schoolsResult)
      setLoading(false)
    }
    asyncFn()
  }, [])
  return (
    <div className="App">
      {loading ? <span>Loading ...</span> : <Schools schools={filteredSchools} initialSchools={schools} setFilteredSchools={setFilteredSchools} />}
    </div>
  );
}

export default App;
