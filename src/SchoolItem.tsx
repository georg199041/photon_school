import { useState } from "react"
import { SchoolItemProps } from "./ISchool"
import { Table } from "@radix-ui/themes";

const SchoolItem = ({ item }: SchoolItemProps) => {
    const [isExpanded, setExpanded] = useState(false)
    return (
    <>
        <Table.Row className="school-item" onClick={() => setExpanded(!isExpanded)}>
            <Table.RowHeaderCell>
            <span>{item.school_name}</span>
          {isExpanded && (
            <div>
              <hr />
              <div>
                <h3>Overview</h3>
                <span>{item.overview_paragraph}</span>
              </div>
              <div>
                <h3>Academic opportunities</h3>
                <ul>
                  <li>{item.academicopportunities1}</li>
                  <li>{item.academicopportunities2}</li>
                </ul>
              </div>
              {(item.admissionspriority11 ||
                item.admissionspriority21 ||
                item.admissionspriority31) && (
                <div>
                  <h3>Admission priority</h3>
                  <ul>
                    <li>{item.admissionspriority11}</li>
                    <li>{item.admissionspriority21}</li>
                    <li>{item.admissionspriority31}</li>
                  </ul>
                </div>
              )}
              <div>
                <h3>
                  Attendance rate{" "}
                  <span>{`${(+item.attendance_rate * 100).toFixed(0)}%`}</span>
                </h3>
              </div>
              <div>
                <h3>BBL</h3>
                <span>{item.bbl}</span>
              </div>
              <div>
                <h3>BIN</h3>
                <span>{item.bin}</span>
              </div>
              <div>
                <h3>BORO</h3>
                <span>{item.boro}</span>
              </div>
              <div>
                <h3>Borought</h3>
                <span>{item.borough}</span>
              </div>
              <div>
                <h3>Building code</h3>
                <span>{item.building_code}</span>
              </div>
              <div>
                <h3>Bus</h3>
                <span>{item.bus}</span>
              </div>
              <div>
                <h3>Census tract</h3>
                <span>{item.census_tract}</span>
              </div>
              <div>
                <h3>City</h3>
                <span>{item.city}</span>
              </div>
              <div>
                <h3>code 1</h3>
                <span>{item.code1}</span>
              </div>
              <div>
                <h3>Council district</h3>
                <span>{item.council_district}</span>
              </div>
              <div>
                <h3>Directions</h3>
                <span>{item.directions1}</span>
              </div>
              <div>
                <h3>El programs</h3>
                <span>{item.ell_programs}</span>
              </div>
              <div>
                <h3>Extracircular activities</h3>
                <span>{item.extracurricular_activities}</span>
              </div>
              <div>
                <h3>Fax number</h3>
                <span>{item.fax_number}</span>
              </div>
              <div>
                <h3>Final grades</h3>
                <span>{item.finalgrades}</span>
              </div>
              <div>
                <h3>Grade applicants</h3>
                <ul>
                  <li>9G applicants: {item.grade9geapplicants1}</li>
                  <li>
                    9G applicants per seat: {item.grade9geapplicantsperseat1}
                  </li>
                  <li>
                    9G applicants filled:{" "}
                    {item.grade9gefilledflag1 === "Y" ? "Yes" : "No"}
                  </li>
                  <li>9G swd applicants: {item.grade9swdapplicants1}</li>
                  <li>
                    9G swd applicants per seat:{" "}
                    {item.grade9swdapplicantsperseat1}
                  </li>
                  <li>
                    9G swd applicants filled:{" "}
                    {item.grade9swdfilledflag1 === "Y" ? "Yes" : "No"}
                  </li>
                  <li>Grades 2018: {item.grades2018}</li>
                  <li>Grades 2018: {item.grades2018}</li>
                </ul>
              </div>
              <div>
                <h3>Interest</h3>
                <span>{item.interest1}</span>
              </div>
              <div>
                <h3>Location</h3>
                <span>{item.location}</span>
              </div>
              <div>
                <h3>Neighborhood</h3>
                <span>{item.neighborhood}</span>
              </div>
              <div>
                <h3>NTA</h3>
                <span>{item.nta}</span>
              </div>
              <div>
                <h3>Offer rate</h3>
                <span>{item.offer_rate1}</span>
              </div>
              <div>
                <h3>PCT STU</h3>
                <div>Enough veriety: {item.pct_stu_enough_variety}</div>
                <div>Safe: {item.pct_stu_safe}</div>
              </div>
              <div>
                <h3>Phone number</h3>
                <span>{item.phone_number}</span>
              </div>
              <div>
                <h3>Primary address</h3>
                <span>{item.primary_address_line_1}</span>
              </div>
              <div>
                <h3>Programm</h3>
                <span>{item.program1}</span>
              </div>
              {(item.requirement1_1 ||
                item.requirement2_1 ||
                item.requirement3_1 ||
                item.requirement4_1 ||
                item.requirement5_1) && (
                <div>
                  <h3>Requirements</h3>
                  <ul>
                    <li>{item.requirement1_1}</li>
                    <li>{item.requirement2_1}</li>
                    <li>{item.requirement3_1}</li>
                    <li>{item.requirement4_1}</li>
                    <li>{item.requirement5_1}</li>
                  </ul>
                </div>
              )}
              <div>
                <h3>School details</h3>
                <ul>
                  <li>10th seats: {item.school_10th_seats}</li>
                  <li>9th seats: {item.seats9ge1}</li>
                  <li>9th swd seats: {item.seats9swd1}</li>
                  <li>seats 101: {item.seats101}</li>
                  <li>
                    accessibility description:{" "}
                    {item.school_accessibility_description}
                  </li>
                  <li>Email: {item.school_email}</li>
                  <li>Sports: {item.school_sports}</li>
                  <li>State: {item.state_code}</li>
                  <li>Subway: {item.subway}</li>
                  <li>Total students: {item.total_students}</li>
                  <li>Total students: {item.total_students}</li>
                  <li>Website: {item.website}</li>
                  <li>ZIP: {item.zip}</li>
                </ul>
              </div>
            </div>
          )}
            </Table.RowHeaderCell>
            <Table.Cell>{item.dbn}</Table.Cell>
        </Table.Row>
    </>
      
    );
}

export default SchoolItem